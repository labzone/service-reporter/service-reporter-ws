<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;

/**
 * @NamePrefix("user_")
 * @RouteResource("User")
 */
class UserController extends Controller
{
    /**
     * @ApiDoc(
     * 	resource=true,
     * 	description="Get contents filter by category",
     *     section="User"
     * )
     */
    public function putAction(Request $request)
    {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new \FOS\UserBundle\Event\GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(\FOS\UserBundle\FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        var_dump($form->getData());

        if ($form->isValid()) {
            $event = new \FOS\UserBundle\Event\FormEvent($form, $request);
            $dispatcher->dispatch(\FOS\UserBundle\FOSUserEvents::REGISTRATION_SUCCESS, $event);

            $userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_registration_confirmed');
                $response = new \Symfony\Component\HttpFoundation\RedirectResponse($url);
            }

            $dispatcher->dispatch(\FOS\UserBundle\FOSUserEvents::REGISTRATION_COMPLETED, new \FOS\UserBundle\Event\FilterUserResponseEvent($user, $request, $response));

            $view = $this->view(array('token' => $this->get('lexik_jwt_authentication.jwt_manager')->create($user)), Codes::HTTP_CREATED);

            return $this->handleView($view);
        }

        throw new \Exception('Error Processing Request', 1);
        $view = $this->view($form, \FOS\RestBundle\Util\Codes::HTTP_BAD_REQUEST);

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *      resource=true,
     *      description="Update user",
     *      section="User"
     * )
     */
    public function patchAction()
    {
    }

    /**
     * @ApiDoc(resource=true, description="Get all users", section="User")
     */
    public function cgetAction()
    {
    }

    /**
     * @ApiDoc(resource=true, description="Get a user", section="User")
     */
    public function getAction()
    {
    }
}
