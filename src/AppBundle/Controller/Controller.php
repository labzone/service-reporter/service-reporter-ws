<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

abstract class Controller extends FOSRestController
{
    /**
     * get entity manager.
     *
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->get('doctrine')->getManager();
    }

    public function optionsAction()
    {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, PATCH, POST, PUT');

        return $response;
    }

    /*public abstract function cgetAction();
    public abstract function deleteAction($id);
    public abstract function getAction($id);
    public abstract function postAction(Request $request);
    public abstract function putAction($id);*/
}
