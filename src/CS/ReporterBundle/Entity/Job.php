<?php

namespace CS\ReporterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\User;

/**
 * Job.
 *
 * @ORM\Table(name="job")
 * @ORM\Entity(repositoryClass="CS\ReporterBundle\Repository\JobRepository")
 */
class Job
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_in", type="time")
     */
    private $timeIn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_out", type="time")
     */
    private $timeOut;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="JobType")
     *     @ORM\JoinTable(
     *         name="job_job_type",
     *         joinColumns={@ORM\JoinColumn(
     *             name="job_id",
     *             referencedColumnName="id"
     *         )},
     *         inverseJoinColumns={@ORM\JoinColumn(
     *             name="type_id",
     *             referencedColumnName="id"
     *         )}
     *      )
     */
    private $types;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="string", length=255)
     */
    private $notes;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="Customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private $customer;

    /**
     * @var Signature
     *
     * @ORM\ManyToOne(targetEntity="Signature")
     * @ORM\JoinColumn(name="signature_id", referencedColumnName="id")
     */
    private $signature;

    /**
     * @ORM\ManyToMany(targetEntity="Task")
     *     @ORM\JoinTable(
     *         name="job_task",
     *         joinColumns={@ORM\JoinColumn(
     *             name="job_id",
     *             referencedColumnName="id"
     *         )},
     *         inverseJoinColumns={@ORM\JoinColumn(
     *             name="task_id",
     *             referencedColumnName="id"
     *         )}
     *      )
     */
    private $tasks;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    public function __construct()
    {
        $this->tasks = new ArrayCollection();
        $this->types = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timeIn.
     *
     * @param \DateTime $timeIn
     *
     * @return Job
     */
    public function setTimeIn($timeIn)
    {
        $this->timeIn = $timeIn;

        return $this;
    }

    /**
     * Get timeIn.
     *
     * @return \DateTime
     */
    public function getTimeIn()
    {
        return $this->timeIn;
    }

    /**
     * Set timeOut.
     *
     * @param \DateTime $timeOut
     *
     * @return Job
     */
    public function setTimeOut($timeOut)
    {
        $this->timeOut = $timeOut;

        return $this;
    }

    /**
     * Get timeOut.
     *
     * @return \DateTime
     */
    public function getTimeOut()
    {
        return $this->timeOut;
    }

    public function getCustomer()
    {
        return $this->customer;
    }

    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    public function setTasks(ArrayCollection $tasks)
    {
        $this->tasks = $tasks;

        return $this;
    }

    public function getTasks()
    {
        return $this->tasks;
    }

    public function addTask(Task $task)
    {
        $this->tasks->add($task);

        return $this;
    }

    public function removeTask(Task $task)
    {
        $this->tasks->removeElement($task);

        return $this;
    }

    /**
     * Gets the value of signature.
     *
     * @return Signature
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Sets the value of signature.
     *
     * @param Signature $signature the signature
     *
     * @return self
     */
    public function setSignature(Signature $signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * Gets the value of user.
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Sets the value of user.
     *
     * @param User $user the user
     *
     * @return self
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }
}
