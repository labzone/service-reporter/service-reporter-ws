<?php

namespace CS\ReporterBundle\Controller;

use AppBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use CS\ReporterBundle\Form\JobType;

/**
 * @NamePrefix("job_type_")
 * @RouteResource("JobType")
 */
class JobTypeController extends Controller
{
    /**
     * @ApiDoc(resource=true, description="Get all jobs", section="JobType")
     */
    public function cgetAction()
    {
        return $this->getEntityManager()
            ->getRepository('CSReporterBundle:JobType')
            ->findAll();
    }
}
