<?php

namespace CS\ReporterBundle\Controller;

use AppBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use CS\ReporterBundle\Entity\Customer;
use CS\ReporterBundle\Form\CustomerType;

/**
 * @NamePrefix("customer_")
 * @RouteResource("Customer")
 */
class CustomerController extends Controller
{
    /**
     * @ApiDoc(
     * 	resource=true,
     * 	description="Update",
     *  section="Customer",
     *  input = "CS\ReporterBundle\Form\CustomerType",
     * )
     */
    public function putAction(Request $request, $id)
    {
        $entityManager = $this->getEntityManager();
        $customer = $entityManager
            ->getRepository('CSReporterBundle:Customer')
            ->find($id);

        $form = $this->createForm(new CustomerType(), $customer, [
            'method' => 'PUT',
        ]);
        $form->bind($request);
        if ($form->isValid()) {
            $entity = $form->getData();
            $entityManager->persist($entity);
            $entityManager->flush();

            return $entity;
        }
        throw new \Exception('Error Processing Request', 1);
    }

    /**
     * @ApiDoc(
     * 		resource=true,
     * 		description="Create new",
     *      section="Customer",
     *      input = "CS\ReporterBundle\Form\CustomerType",
     * )
     */
    public function postAction(Request $request)
    {
        $form = $this->createForm(new CustomerType(), new Customer());
        $form->bind($request);

        if ($form->isValid()) {
            $entity = $form->getData();
            $entityManager = $this->getEntityManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            return $entity;
        }
    }

    /**
     * @ApiDoc(resource=true, description="Get all", section="Customer")
     */
    public function cgetAction()
    {
        return $this->getEntityManager()
            ->getRepository('CSReporterBundle:Customer')
            ->findAll();
    }

    /**
     * @ApiDoc(resource=true, description="Get one", section="Customer")
     */
    public function getAction($id)
    {
        return $this->getEntityManager()
            ->getRepository('CSReporterBundle:Customer')
            ->find($id);
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     description="Delete a customer",
     *     section="Customer"
     * )
     */
    public function deleteAction($id)
    {
        $entityManager = $this->getEntityManager();
        $customer = $entityManager
            ->getRepository('CSReporterBundle:Customer')
            ->find($id);
        $entityManager->remove($customer);
        $entityManager->flush();
    }
}
