<?php

namespace CS\ReporterBundle\Controller;

use AppBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use CS\ReporterBundle\Entity\Job;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use CS\ReporterBundle\Form\JobType;

/**
 * @NamePrefix("job_")
 * @RouteResource("Job")
 */
class JobController extends Controller
{
    /**
     * @View()
     * @ApiDoc(
     * 	resource=true,
     * 	description="Creates new job",
     * 	section="Job",
     *  input = "CS\ReporterBundle\Form\JobType",
     * )
     * @ParamConverter("job", converter="fos_rest.request_body")
     */
    public function postAction(Request $request)
    {
        $form = $this->createForm(new JobType(), new Job());
        $form->bind($request);

        if ($form->isValid()) {
            $entity = $form->getData();
            $entityManager = $this->getEntityManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            return $entity;
        }
    }

    /**
     * @ApiDoc(
     * 		resource=true,
     * 		description="Update job",
     * 		section="Job"
     * )
     */
    public function putAction(Request $request, $id)
    {
        $entityManager = $this->getEntityManager();
        $job = $entityManager
            ->getRepository('CSReporterBundle:Job')
            ->find($id);

        $form = $this->createForm(new JobType(), $job, [
            'method' => 'PUT',
        ]);
        $form->bind($request);
        if ($form->isValid()) {
            $entity = $form->getData();
            var_dump($entity);
            /*$entityManager->persist($entity);
            $entityManager->flush();*/

            return $entity;
        }
        throw new \Exception('Error Processing Request', 1);
    }

    /**
     * @ApiDoc(resource=true, description="Get all jobs", section="Job")
     */
    public function cgetAction()
    {
        return $this->getEntityManager()
            ->getRepository('CSReporterBundle:Job')
            ->findAll();
    }

    /**
     * @ApiDoc(resource=true, description="Get a job", section="Job")
     */
    public function getAction($id)
    {
        return $this->getEntityManager()
            ->getRepository('CSReporterBundle:Job')
            ->find($id);
    }

    public function deleteAction($id)
    {
        $entityManager = $this->getEntityManager();
        $job = $entityManager
            ->getRepository('CSReporterBundle:Job')
            ->find($id);
        $entityManager->remove($job);
        $entityManager->flush();
    }
}
