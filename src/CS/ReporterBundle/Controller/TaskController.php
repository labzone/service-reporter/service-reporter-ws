<?php

namespace CS\ReporterBundle\Controller;

use AppBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use CS\ReporterBundle\Entity\Task;
use CS\ReporterBundle\Form\TaskType;

/**
 * @NamePrefix("task_")
 * @RouteResource("Task")
 */
class TaskController extends Controller
{
    /**
     * @ApiDoc(
     * 	   resource=true,
     * 	   description="Put new job",
     *     section="Task",
     *     input = "CS\ReporterBundle\Form\TaskType",
     * )
     */
    public function putAction(Request $request, $id)
    {
        $entityManager = $this->getEntityManager();
        $task = $entityManager
            ->getRepository('CSReporterBundle:Task')
            ->find($id);

        $form = $this->createForm(new TaskType(), $task, [
            'method' => 'PUT',
        ]);
        $form->bind($request);
        if ($form->isValid()) {
            $entity = $form->getData();
            $entityManager->persist($entity);
            $entityManager->flush();

            return $entity;
        }
        throw new \Exception('Error Processing Request', 1);
    }

    /**
     * @ApiDoc(
     *      resource=true,
     *      description="Create new",
     *      section="Task",
     *      input = "CS\ReporterBundle\Form\TaskType",
     * )
     */
    public function postAction(Request $request)
    {
        $form = $this->createForm(new TaskType(), new Task());
        $form->bind($request);

        if ($form->isValid()) {
            $entity = $form->getData();
            $entityManager = $this->getEntityManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            return $entity;
        }
    }

    /**
     * @ApiDoc(
     * 		resource=true,
     * 		description="Update job",
     *      section="Task"
     * )
     */
    public function patchAction()
    {
    }

    /**
     * @ApiDoc(resource=true, description="Get all tasks", section="Task")
     */
    public function cgetAction()
    {
        return $this->getEntityManager()
            ->getRepository('CSReporterBundle:Task')
            ->findBy([], ['position' => 'ASC']);
    }

    /**
     * @ApiDoc(resource=true, description="Get a task", section="Task")
     */
    public function getAction($id)
    {
        return $this->getEntityManager()
            ->getRepository('CSReporterBundle:Task')
            ->find($id);
    }

    /**
     * @ApiDoc(resource=true, description="Delete a task", section="Task")
     */
    public function deleteAction($id)
    {
        $entityManager = $this->getEntityManager();
        $task = $entityManager
            ->getRepository('CSReporterBundle:Task')
            ->find($id);
        $entityManager->remove($task);
        $entityManager->flush();
    }
}
