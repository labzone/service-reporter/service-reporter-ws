<?php

namespace CS\ReporterBundle\Controller;

use AppBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use CS\ReporterBundle\Entity\Company;
use CS\ReporterBundle\Form\CompanyType;

/**
 * @NamePrefix("company_")
 * @RouteResource("Company")
 */
class CompanyController extends Controller
{
    /**
     * @ApiDoc(resource=true, description="Get all", section="Company")
     */
    public function cgetAction()
    {
        return $this
            ->getEntityManager()
            ->getRepository('CSReporterBundle:Company')
            ->findAll();
    }

    /**
     * @ApiDoc(resource=true, description="Get one", section="Company")
     */
    public function getAction($id)
    {
        return $this
            ->getEntityManager()
            ->getRepository('CSReporterBundle:Company')
            ->find($id);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Update",
     *  section="Company",
     *  input = "CS\ReporterBundle\Form\CompanyType",
     * )
     */
    public function putAction(Request $request, $id)
    {
        $entityManager = $this->getEntityManager();
        $company = $entityManager
            ->getRepository('CSReporterBundle:Company')
            ->find($id);

        $form = $this->createForm(new CompanyType(), $company, [
            'method' => 'PUT',
        ]);
        $form->bind($request);
        if ($form->isValid()) {
            $entity = $form->getData();
            $entityManager->persist($entity);
            $entityManager->flush();

            return $entity;
        }

        throw new \Exception('Error Processing Request', 1);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Update",
     *  section="Company",
     *  input = "CS\ReporterBundle\Form\CompanyType",
     * )
     */
    public function postAction(Request $request)
    {
        $entityManager = $this->getEntityManager();
        $company = $entityManager
            ->getRepository('CSReporterBundle:Company')
            ->findOneBy([]);

        $form = $this->createForm(new CompanyType(), $company);
        $form->bind($request);

        if ($form->isValid()) {
            $entity = $form->getData();
            $entityManager->persist($entity);
            $entityManager->flush();

            return $entity;
        }
    }
}
