<?php

namespace CS\ReporterBundle\Command;

use CS\ReporterBundle\Entity\Company;
use CS\ReporterBundle\Entity\Contact;
use CS\ReporterBundle\Entity\Customer;
use PHPExcel_Worksheet;
use PHPExcel_Worksheet_RowCellIterator;
use PHPExcel_Worksheet_CellIterator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Collections\ArrayCollection;

class CustomerImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('customer:import')
            ->setDescription('import customer from excel file')
            ->addArgument('path', InputArgument::REQUIRED, 'excel file path')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('path');
        $excelObject = $this->getContainer()->get('phpexcel')->createPHPExcelObject($path);
        $worksheet = $excelObject->getWorksheetIterator();
        $customers = $this->getCompanyData($worksheet->current());
        $worksheet->next();
        $customers = array_merge($customers, $this->getIndividualData($worksheet->current()));
        $this->saveCustomers($customers);
        $output->writeln('Command result.');
    }

    private function getCompanyData(PHPExcel_Worksheet $worksheet)
    {
        $customers = [];
        foreach ($worksheet->getRowIterator() as $row) {
            if ($row->getRowIndex() < 3) {
                continue;
            }
            $cellIterator = $row->getCellIterator();
            $shortName = self::getCurrentCellValue($cellIterator);
            $cellIterator->next();
            $name = self::getCurrentCellValue($cellIterator);
            $cellIterator->next();
            $address = self::getCurrentCellValue($cellIterator);
            $cellIterator->next();
            $vatNumber = self::getCurrentCellValue($cellIterator);
            $cellIterator->next();
            $brnNumber = self::getCurrentCellValue($cellIterator);
            $cellIterator->next();
            $phone = self::getCurrentCellValue($cellIterator);
            $cellIterator->next();
            $contactName = self::getCurrentCellValue($cellIterator);
            $cellIterator->next();
            $title = self::getCurrentCellValue($cellIterator);
            $cellIterator->next();
            $email = self::getCurrentCellValue($cellIterator);
            if (!($name && $address)) {
                continue;
            }
            $contacts = $this->getContacts($cellIterator);
            $customer = new Customer();
            $customer
                ->setType(Customer::COMPANY)
                ->setShortName($shortName)
                ->setName($name)
                ->setAddress($address)
                ->setVatNumber($vatNumber)
                ->setBrnNumber($brnNumber)
                ->setPhone($phone)
                ->setContacts($contacts);
            $customers[] = $customer;
        }

        return $customers;
    }

    private function getContact(PHPExcel_Worksheet_RowCellIterator $cellIterator, $index)
    {
        $phone = self::getCurrentCellValue($cellIterator->seek(chr(ord($index) - 3)));
        if ($index === 'l') {
            $phone = self::getCurrentCellValue($cellIterator->seek(chr(ord($index) - 5)));
        }
        $name = self::getCurrentCellValue($cellIterator->seek($index++));
        $title = self::getCurrentCellValue($cellIterator->seek($index++));
        $email = self::getCurrentCellValue($cellIterator->seek($index++));
        $contact = new Contact();
        $contact
            ->setTitle($title)
            ->setName($name)
            ->setEmail($email)
            ->setPhone($phone);

        return $contact;
    }

    private function getIndividualData(PHPExcel_Worksheet $worksheet)
    {
        $customers = [];
        foreach ($worksheet->getRowIterator() as $row) {
            if ($row->getRowIndex() < 3) {
                continue;
            }
            $cellIterator = $row->getCellIterator();
            $shortName = self::getCurrentCellValue($cellIterator);
            $cellIterator->next();
            $name = self::getCurrentCellValue($cellIterator);
            $cellIterator->next();
            $address = self::getCurrentCellValue($cellIterator);
            $cellIterator->next();
            $phone = self::getCurrentCellValue($cellIterator);
            $cellIterator->next();
            $cellIterator->next();
            $email = self::getCurrentCellValue($cellIterator);
            if (!($shortName && $name && $address && $phone && $email)) {
                continue;
            }
            $customer = new Customer();
            $customer
                ->setType(Customer::INDIVIDUAL)
                ->setShortName($shortName)
                ->setName($name)
                ->setAddress($address)
                ->setPhone($phone)
                ->setEmail($email);
            $customers[] = $customer;
        }

        return $customers;
    }

    private static function getCurrentCellValue(PHPExcel_Worksheet_RowCellIterator $cellIterator)
    {
        return $cellIterator->current()->getValue();
    }

    private function getContacts(PHPExcel_Worksheet_CellIterator $cellIterator)
    {
        $contacts = [];
        $contacts[] = $this->getContact($cellIterator, 'i');
        $contacts[] = $this->getContact($cellIterator, 'l');

        return new ArrayCollection($contacts);
    }

    private function saveCustomers($customers)
    {
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        foreach ($customers as $customer) {
            $entityManager->persist($customer);
        }
        $entityManager->flush();
    }
}
