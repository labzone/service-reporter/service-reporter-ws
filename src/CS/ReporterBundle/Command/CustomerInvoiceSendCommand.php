<?php

namespace CS\ReporterBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CustomerInvoiceSendCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('customer:invoice:send')
            ->setDescription('Send customer invoice')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
    }
}
