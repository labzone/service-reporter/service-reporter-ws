<?php

namespace CS\ReporterBundle\Form;

use Symfony\Component\Form\AbstractType as BaseType;

class AbstractType extends BaseType
{
    public function getDefaults()
    {
        return array(
            'csrf_protection' => false,
            'cascade_validation' => true,
        );
    }

    public function getName()
    {
        return '';
    }
}
