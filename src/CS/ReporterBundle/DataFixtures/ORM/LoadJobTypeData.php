<?php

namespace CS\ReporterBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use CS\ReporterBundle\Entity\JobType;

class LoadJobTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->getTypes() as $type) {
            $jobType = new JobType();
            $jobType->setContent($type);
            $manager->persist($jobType);
        }

        $manager->flush();
    }

    private function getTypes()
    {
        return ['warranty', 'maintenance', 'changeable'];
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 2;
    }
}
