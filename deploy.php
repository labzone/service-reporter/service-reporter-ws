<?php

// All Deployer recipes are based on `recipe/common.php`.
require 'recipe/symfony.php';

serverList('app/config/servers.yml');

set('repository', 'git@gitlab.com:voidify/service-reporter-ws.git');
set('http_user', 'csr');
set('writable_use_sudo', false);
set('writable_dirs', ['app/cache', 'app/logs', 'web/media']);
set('shared_dirs', ['app/logs', 'web/media']);
set('dump_assets', false);
set('keep_releases', 3);

// install composer
task('deploy:install:composer', function(){
    cd('{{release_path}}');

    run('curl -sS https://getcomposer.org/installer | php');
});

before('deploy:vendors', 'deploy:install:composer');
